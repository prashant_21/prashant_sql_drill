CREATE TABLE doctor(
    Doctor_ID INTEGER PRIMARY KEY,
    Doctor_Name TEXT NOT NULL,
    Secretary_Name TEXT,
    Patient_No INTEGER NOT NULL,
    FOREIGN KEY (Patient_No) REFERENCES patient (Patient_No), 
    Prescription_No INTEGER NOT NULL,
    FOREIGN KEY (Prescription_No) REFERENCES prescription (Prescription_No),
);

CREATE TABLE patient(
    Patient_No INTEGER PRIMARY KEY,
    Patient_Name TEXT NOT NULL,
    Patient_DOB DATE NOT NULL,
    Patient_Address TEXT NOT NULL
);

CREATE TABLE prescription(
    Prescription_No INTEGER PRIMARY KEY,
    Prescription_Date DATE NOT NULL,   
    Drug TEXT NOT NULL,
    Dosage TEXT NOT NULL
);