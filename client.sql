CREATE TABLE client (
    Client_No INTEGER PRIMARY KEY,
    Client_Name TEXT NOT NULL,
    Client_Location TEXT NOT NULL
);

CREATE TABLE manager(
    Manager_No INTEGER PRIMARY KEY,
    Manager_Name TEXT NOT NULL,
    Manager_Location TEXT NOT NULL
);

CREATE TABLE contract(
    Contract_No INTEGER PRIMARY KEY,
    Estimated_Cost INTEGER NOT NULL,
    Completion_Date DATE 
);

CREATE TABLE staff(
    Staff_No INTEGER PRIMARY KEY,
    Staff_Name TEXT NOT NULL,
    Staff_Location TEXT NOT NULL
);

CREATE TABLE project(
    Contract_No INTEGER PRIMARY KEY,
    FOREIGN KEY (Contract_No) REFERENCES contract (Contract_No),
    Client_No INTEGER NOT NULL,
    FOREIGN KEY (Client_No) REFERENCES client (Client_No),
    Manager_No INTEGER NOT NULL,
    FOREIGN KEY (Manager_No) REFERENCES manager (Manager_No),
    Staff_No INTEGER NOT NULL,
    FOREIGN KEY (Staff_No) REFERENCES staff (Staff_No)
);