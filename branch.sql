CREATE TABLE branch(
    Branch_No INTEGER PRIMARY KEY, 
    Branch_Address TEXT NOT NULL
);

CREATE TABLE author(
    Author_ID INTEGER PRIMARY KEY AUTO_INCREMENT,
    Author_Name TEXT NOT NULL
);

CREATE TABLE publisher(
    Publisher_ID INTEGER PRIMARY KEY AUTO_INCREMENT,
    Publisher_Name TEXT NOT NULL
);

CREATE TABLE book(
    ISBN INTEGER PRIMARY KEY,
    Title TEXT NOT NULL,
    Author_ID INTEGER NOT NULL,
    FOREIGN KEY (Author_ID) REFERENCES author(Author_ID),
    Publisher_ID INTEGER NOT NULL,
    FOREIGN KEY (Publisher_ID) REFERENCES publisher(Publisher_ID),
    Num_Copies INTEGER NOT NULL,
    Branch_No INTEGER NOT NULL,
    FOREIGN KEY (Branch_No) REFERENCES branch(Branch_No)
);